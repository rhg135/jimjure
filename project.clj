(defproject jimjure "0.0.1-SNAPSHOT"
  :description "A JIM server in clojure"
  :url "http://jim.hackpad.com"
  :license {:name "MIT"
            :url "http://www.opensource.org/licenses/MIT"}
  :main jimjure.core
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [com.taoensso/timbre "3.0.0-RC2"]
                 [org.clojure/tools.trace "0.7.6"]
                 [org.clojure/tools.reader "0.8.3"]
                 [cheshire "5.3.0"]])
