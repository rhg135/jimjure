; This file is part of jimjure

(ns jimjure.server
  "A better server for java"
  (:import [java.net ServerSocket]
           [java.util Date])
  (:require [clojure.java.io :as io]
            [taoensso.timbre :as t]))

(defn start-server
  "Runs the f on every connection to socket with a map of info"
  [^ServerSocket socket f]
  (while (not (.isClosed socket))
    (let [client (.accept socket)
          _ (t/debug "Got client" client)
          address (-> client
                      .getInetAddress
                      .getHostAddress)
          _ (t/debug "Got address" address)
          port (. client
                  getPort)
          _ (t/debug "Got port" port)
          in (io/reader client)
          _ (t/debug "Got reader" in)
          out (io/writer client)
          _ (t/debug "Got writer" out)
          info {:in in
                :out out
                :time (Date.)
                :id (str address port)
                :close! #(.close client)}]
      (t/debug (str "("f info")"))
      (future
        (try
          (f info)
          (finally
            (.close client)))))))
