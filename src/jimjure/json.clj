; This file is part of jimjure
(ns jimjure.json
  "Helpers for JSON sockets"
  (:import java.io.Writer)
  (:require [cheshire.core :as json]
            [taoensso.timbre :as t]))

(defn- write!
  "Writes and flushes returning the arg"
  [^Writer out ^String arg]
  (doto out
    (.write arg)
    .flush)
  arg)

(defn json-handler
  "Interprets all input and output as JSON"
  [f]
  (fn [info]
    (let [{:keys [in out close!]} info
          obj-seq (json/parsed-seq in true)]
      (future (t/debug "Got" obj-seq "from" info))
      (doseq [obj obj-seq]
        (let [r (f obj)
              _ (t/debug "Result:" r)
              js (str (json/generate-string r) \newline)
              _ (t/debug "JSON:" js)]
          (t/debug (str "(.write out js)"))
          (write! out js)))
      (close!))))
