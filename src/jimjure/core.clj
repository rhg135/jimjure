; This file is part of jimjure

(ns jimjure.core
  "Starts the server"
  (:gen-class)
  (:import [java.net ServerSocket InetAddress])
  (:require [jimjure.address :as net]
            [jimjure.server :refer [start-server]]
            [jimjure.json :refer [json-handler]]
            [jimjure.digest :as d]
            [clojure.java.io :as io]
            [clojure.tools.reader.edn :as edn]
            [clojure.string :as s]
            [clojure.tools.trace :as t]))

(def state (atom {:users []}))

(defn log-in!
  "Logs in to the server"
  [name _]
  (swap! state update-in [:users] conj name))

(defmulti handle (comp keyword :action))

(defmethod handle :authenticate
  [obj]
  (let [{{name :account_name pass :password} :user} obj]
    (try
      (log-in! state name pass)
      {:response 200
       :alert "It's all good."}
      (catch Exception _
        {:response 402
         :error "Erm, try again"
         }))))

(defmethod handle :kill
  [_]
  {:response 200
   :alert "BANG!"})

(defmethod handle :default
  [obj]
  {:response 400
   :error "Unknown request."
   :object obj})

(defn jim-server
  [^ServerSocket server]
  (start-server server (partial json-handler handle)))

(defn -main
  [& args]
  (let [{:keys [port ip digester]}
               (edn/read-string (slurp (io/resource "config.clj")))
        port (or (net/port? port)
                 4004)
        ip (or (net/address ip) (net/address "127.0.0.1"))
        server (ServerSocket. port 10 ip)]
    (binding [d/*options* (reduce (fn [m [k v]] (assoc m k v)) d/*options* digester)]
      (jim-server server))))
