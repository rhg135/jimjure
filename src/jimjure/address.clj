; This file is part of jimjure

(ns jimjure.address
  "Functions to deal with InetAdress es"
  (:import [java.net ServerSocket InetAddress])
  (:require [clojure.string :as s]))

(defn ^InetAddress ipv4
  "Parses an ipv4 string"
  [ip]
  (InetAddress/getByAddress (byte-array
                              (map byte
                                   (map #(Integer/parseInt %) (s/split ip #"\."))))))

(defn ^InetAddress address
  "Returns an InetAddress

  Resolves strings as hostnames or ipv4

  Examples:
  (address \"hostname.org\") (address \"127.0.0.1\")"
  [addr]
  (cond
    (string? addr) (or (ipv4 addr) (InetAddress/getByName addr))))

(defn address?
  "Is this an address?"
  [obj]
  (= InetAddress (class obj)))

(defn port?
  "Is this a valid port?"
  [n]
  (when (< 0 n 0x10000)
    n))
