; This file is part of jimjure
(ns jimjure.digest
  "Deals with passwords"
  (:import [java.security SecureRandom MessageDigest]
           org.apache.commons.codec.binary.Base64
           java.nio.charset.Charset))

; Data types
(defn charset
  [encoding]
  (Charset/forName (name encoding)))

(defn message-digest
  [algorithm]
  (MessageDigest/getInstance (name algorithm)))

; Salt
(defn add-salt
  [salt pass]
  (byte-array
    (concat (seq salt) (seq pass))))

(defn make-salt
  [n]
  (-> SecureRandom new (.generateSeed n)))

; Codecs
(defn base64-encode
  [in]
  (Base64/encodeBase64 in))

(defn base64-decode
  [in]
  (Base64/decodeBase64 in))

(defn from-bytes
  [encoding in]
  (String. in encoding))

(defn get-bytes
  [encoding in]
  (.getBytes in encoding))

; Digesting
(defn digest
  [^MessageDigest md in]
  (.digest md in))

; Passwords
(def ^:dynamic *options*
  {:algorithm :sha-256
   :iterations 20000
   :encoding :utf-8
   :salt-size 64})

(def ascii
  (charset :us-ascii))

(defn digest-password
  "Returns a base64 encoded ascii string from a salt and a string"
  [^String salt ^String in]
  (println "Salt:" salt "In:" in)
  (let [algorithm (message-digest (or (:algorithm *options*) :sha-256))
        n (:iterations *options*)
        nacl (->> (get-bytes ascii salt) base64-decode)
        bytes (get-bytes (-> *options* :encoding charset) in)]
    (->> (add-salt nacl bytes)
              (iterate #(digest algorithm %))
              (take n)
              last
         base64-encode
         (from-bytes ascii))))

(defn make-password
  "Takes a string and returns a map with a new salt as base64 encoded string
  and a password as a ascii base64 string"
  [pass]
  (let [salt (-> (:salt-size *options*) make-salt)
        nacl (->> salt
                  base64-encode
                  (from-bytes ascii))]
    {:salt nacl
     :password (digest-password nacl pass)}))
