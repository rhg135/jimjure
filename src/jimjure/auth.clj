; This file is part of jimjure
(ns jimjure.auth
  "Deals with password in a database"
  (:require 
    [clojure.java.jdbc :as sql]
    [taoensso.timbre :as t]
    [jimjure.digest :as d]))

(def users
  {:subprotocol "h2"
   :subname "resources/users"})

; Table creation
(defn make-tables*
  []
  [(sql/create-table-ddl :users
                         [:id "INT" "AUTO_INCREMENT PRIMARY KEY"]
                         [:name "VARCHAR(255)"])
   (sql/create-table-ddl :passwords
                         [:id "INT" "AUTO_INCREMENT PRIMARY KEY"]
                         [:users_id "INT"]
                         [:salt "VARCHAR(128)"]
                         [:password "VARCHAR(255)"])])

(defn make-tables
  [spec]
  (sql/with-db-connection [db-con spec]
    (doall (map (partial sql/db-do-commands db-con) (make-tables*)))))

; Getting an user
(defn- get-user*
  [con acc]
  (first
    (sql/query con ["SELECT * FROM users WHERE name = ?" acc])))

(defn- get-password*
  [con id]
  (first
    (sql/query con ["SELECT * FROM passwords WHERE users_id = ?" id])))

(defn get-password
  [acc]
  (sql/with-db-transaction [db users]
    (let [{uid :id} (get-user* db acc)
          {:keys [salt password]} (get-password* db uid)
          nacl (->> (d/get-bytes d/ascii salt)
                    d/base64-decode)]
      (when password
        {:salt salt
         :password password}))))

; User adding
(defn add-user
  [name pass]
  (sql/with-db-transaction [db users]
    (sql/insert! db :users {:name name})
    (let [{uid :id} (get-user* db name)
          record (assoc (d/make-password pass) :users_id uid)]
      (sql/insert! db :passwords record))))

; Magic sauce
(defn log-in
  [name pass]
  (when-let [{:keys [salt password]} (get-password name)]
    (let [digested (d/digest-password salt pass)]
      (t/debug "Salt:" (seq salt))
      (t/debug (str "(= " password " " digested ")"))
    (= password digested))))
